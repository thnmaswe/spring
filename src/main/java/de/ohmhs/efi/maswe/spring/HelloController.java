package de.ohmhs.efi.maswe.spring;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class HelloController {

    private final JdbcTemplate jdbcTemplate;

    public HelloController(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @RequestMapping(path = "/employees", method = RequestMethod.GET)
    public String greet() {
        StringBuilder sb = new StringBuilder();
        sb.append("[\n");
        List<Map<String,Object>> result = jdbcTemplate.queryForList("select FIRST_NAME,LAST_NAME from employees");
        for (Map<String,Object> el : result) {
            sb.append("{lastName:'"+el.get("LAST_NAME").toString()+"', firstName:'"+el.get("FIRST_NAME").toString()+"'}\n");
        }
        sb.append("]");
        return sb.toString();
    }
}
