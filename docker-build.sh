#!/bin/sh
mvn clean package -DskipTests
docker buildx build -t rothmathn/maswe:spring-latest --platform linux/amd64,linux/arm64 --push .